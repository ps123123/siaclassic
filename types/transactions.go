package types

// transaction.go defines the transaction type and all of the sub-fields of the
// transaction, as well as providing helper functions for working with
// transactions. The various IDs are designed such that, in a legal blockchain,
// it is cryptographically unlikely that any two objects would share an id.

import (
	"errors"

	"gitlab.com/moderndevgroup/SiaClassic/build"
	"gitlab.com/moderndevgroup/SiaClassic/crypto"
	"gitlab.com/moderndevgroup/SiaClassic/encoding"
)

const (
	// SpecifierLen is the length in bytes of a Specifier.
	SpecifierLen = 16

	// UnlockHashChecksumSize is the size of the checksum used to verify
	// human-readable addresses. It is not a crypytographically secure
	// checksum, it's merely intended to prevent typos. 6 is chosen because it
	// brings the total size of the address to 38 bytes, leaving 2 bytes for
	// potential version additions in the future.
	UnlockHashChecksumSize = 6
)

// These Specifiers are used internally when calculating a type's ID. See
// Specifier for more details.
var (
	ErrTransactionIDWrongLen = errors.New("input has wrong length to be an encoded transaction id")

	SpecifierClaimOutput          = Specifier{'c', 'l', 'a', 'i', 'm', ' ', 'o', 'u', 't', 'p', 'u', 't'}
	SpecifierFileContract         = Specifier{'f', 'i', 'l', 'e', ' ', 'c', 'o', 'n', 't', 'r', 'a', 'c', 't'}
	SpecifierFileContractRevision = Specifier{'f', 'i', 'l', 'e', ' ', 'c', 'o', 'n', 't', 'r', 'a', 'c', 't', ' ', 'r', 'e'}
	SpecifierMinerFee             = Specifier{'m', 'i', 'n', 'e', 'r', ' ', 'f', 'e', 'e'}
	SpecifierMinerPayout          = Specifier{'m', 'i', 'n', 'e', 'r', ' ', 'p', 'a', 'y', 'o', 'u', 't'}
	SpecifierSiaClassiccoinInput         = Specifier{'s', 'i', 'a', 'c', 'o', 'i', 'n', ' ', 'i', 'n', 'p', 'u', 't'}
	SpecifierSiaClassiccoinOutput        = Specifier{'s', 'i', 'a', 'c', 'o', 'i', 'n', ' ', 'o', 'u', 't', 'p', 'u', 't'}
	SpecifierSiaClassicfundInput         = Specifier{'s', 'i', 'a', 'f', 'u', 'n', 'd', ' ', 'i', 'n', 'p', 'u', 't'}
	SpecifierSiaClassicfundOutput        = Specifier{'s', 'i', 'a', 'f', 'u', 'n', 'd', ' ', 'o', 'u', 't', 'p', 'u', 't'}
	SpecifierStorageProof         = Specifier{'s', 't', 'o', 'r', 'a', 'g', 'e', ' ', 'p', 'r', 'o', 'o', 'f'}
	SpecifierStorageProofOutput   = Specifier{'s', 't', 'o', 'r', 'a', 'g', 'e', ' ', 'p', 'r', 'o', 'o', 'f'}
)

type (
	// A Specifier is a fixed-length byte-array that serves two purposes. In
	// the wire protocol, they are used to identify a particular encoding
	// algorithm, signature algorithm, etc. This allows nodes to communicate on
	// their own terms; for example, to reduce bandwidth costs, a node might
	// only accept compressed messages.
	//
	// Internally, Specifiers are used to guarantee unique IDs. Various
	// consensus types have an associated ID, calculated by hashing the data
	// contained in the type. By prepending the data with Specifier, we can
	// guarantee that distinct types will never produce the same hash.
	Specifier [SpecifierLen]byte

	// IDs are used to refer to a type without revealing its contents. They
	// are constructed by hashing specific fields of the type, along with a
	// Specifier. While all of these types are hashes, defining type aliases
	// gives us type safety and makes the code more readable.

	// TransactionID uniquely identifies a transaction
	TransactionID crypto.Hash
	// SiaClassiccoinOutputID uniquely identifies a siaclassiccoin output
	SiaClassiccoinOutputID crypto.Hash
	// SiaClassicfundOutputID uniquely identifies a siaclassicfund output
	SiaClassicfundOutputID crypto.Hash
	// FileContractID uniquely identifies a file contract
	FileContractID crypto.Hash
	// OutputID uniquely identifies an output
	OutputID crypto.Hash

	// A Transaction is an atomic component of a block. Transactions can contain
	// inputs and outputs, file contracts, storage proofs, and even arbitrary
	// data. They can also contain signatures to prove that a given party has
	// approved the transaction, or at least a particular subset of it.
	//
	// Transactions can depend on other previous transactions in the same block,
	// but transactions cannot spend outputs that they create or otherwise be
	// self-dependent.
	Transaction struct {
		SiaClassiccoinInputs         []SiaClassiccoinInput         `json:"siaclassiccoininputs"`
		SiaClassiccoinOutputs        []SiaClassiccoinOutput        `json:"siaclassiccoinoutputs"`
		FileContracts         []FileContract         `json:"filecontracts"`
		FileContractRevisions []FileContractRevision `json:"filecontractrevisions"`
		StorageProofs         []StorageProof         `json:"storageproofs"`
		SiaClassicfundInputs         []SiaClassicfundInput         `json:"siaclassicfundinputs"`
		SiaClassicfundOutputs        []SiaClassicfundOutput        `json:"siaclassicfundoutputs"`
		MinerFees             []Currency             `json:"minerfees"`
		ArbitraryData         [][]byte               `json:"arbitrarydata"`
		TransactionSignatures []TransactionSignature `json:"transactionsignatures"`
	}

	// A SiaClassiccoinInput consumes a SiaClassiccoinOutput and adds the siaclassiccoins to the set of
	// siaclassiccoins that can be spent in the transaction. The ParentID points to the
	// output that is getting consumed, and the UnlockConditions contain the rules
	// for spending the output. The UnlockConditions must match the UnlockHash of
	// the output.
	SiaClassiccoinInput struct {
		ParentID         SiaClassiccoinOutputID  `json:"parentid"`
		UnlockConditions UnlockConditions `json:"unlockconditions"`
	}

	// A SiaClassiccoinOutput holds a volume of siaclassiccoins. Outputs must be spent
	// atomically; that is, they must all be spent in the same transaction. The
	// UnlockHash is the hash of the UnlockConditions that must be fulfilled
	// in order to spend the output.
	SiaClassiccoinOutput struct {
		Value      Currency   `json:"value"`
		UnlockHash UnlockHash `json:"unlockhash"`
	}

	// A SiaClassicfundInput consumes a SiaClassicfundOutput and adds the siaclassicfunds to the set of
	// siaclassicfunds that can be spent in the transaction. The ParentID points to the
	// output that is getting consumed, and the UnlockConditions contain the rules
	// for spending the output. The UnlockConditions must match the UnlockHash of
	// the output.
	SiaClassicfundInput struct {
		ParentID         SiaClassicfundOutputID  `json:"parentid"`
		UnlockConditions UnlockConditions `json:"unlockconditions"`
		ClaimUnlockHash  UnlockHash       `json:"claimunlockhash"`
	}

	// A SiaClassicfundOutput holds a volume of siaclassicfunds. Outputs must be spent
	// atomically; that is, they must all be spent in the same transaction. The
	// UnlockHash is the hash of a set of UnlockConditions that must be fulfilled
	// in order to spend the output.
	//
	// When the SiaClassicfundOutput is spent, a SiaClassiccoinOutput is created, where:
	//
	//     SiaClassiccoinOutput.Value := (SiaClassicfundPool - ClaimStart) / 10,000 * Value
	//     SiaClassiccoinOutput.UnlockHash := SiaClassicfundInput.ClaimUnlockHash
	//
	// When a SiaClassicfundOutput is put into a transaction, the ClaimStart must always
	// equal zero. While the transaction is being processed, the ClaimStart is set
	// to the value of the SiaClassicfundPool.
	SiaClassicfundOutput struct {
		Value      Currency   `json:"value"`
		UnlockHash UnlockHash `json:"unlockhash"`
		ClaimStart Currency   `json:"claimstart"`
	}

	// An UnlockHash is a specially constructed hash of the UnlockConditions type.
	// "Locked" values can be unlocked by providing the UnlockConditions that hash
	// to a given UnlockHash. See UnlockConditions.UnlockHash for details on how the
	// UnlockHash is constructed.
	UnlockHash crypto.Hash
)

// ID returns the id of a transaction, which is taken by marshalling all of the
// fields except for the signatures and taking the hash of the result.
func (t Transaction) ID() TransactionID {
	// Get the transaction id by hashing all data minus the signatures.
	var txid TransactionID
	h := crypto.NewHash()
	t.marshalSiaClassicNoSignatures(h)
	h.Sum(txid[:0])

	// Sanity check in debug builds to make sure that the ids are going to be
	// the same.
	if build.DEBUG {
		verify := TransactionID(crypto.HashAll(
			t.SiaClassiccoinInputs,
			t.SiaClassiccoinOutputs,
			t.FileContracts,
			t.FileContractRevisions,
			t.StorageProofs,
			t.SiaClassicfundInputs,
			t.SiaClassicfundOutputs,
			t.MinerFees,
			t.ArbitraryData,
		))

		if verify != txid {
			panic("TransactionID is not marshalling correctly")
		}
	}

	return txid
}

// SiaClassiccoinOutputID returns the ID of a siaclassiccoin output at the given index,
// which is calculated by hashing the concatenation of the SiaClassiccoinOutput
// Specifier, all of the fields in the transaction (except the signatures),
// and output index.
func (t Transaction) SiaClassiccoinOutputID(i uint64) SiaClassiccoinOutputID {
	// Create the id.
	var id SiaClassiccoinOutputID
	h := crypto.NewHash()
	h.Write(SpecifierSiaClassiccoinOutput[:])
	t.marshalSiaClassicNoSignatures(h) // Encode non-signature fields into hash.
	encoding.WriteUint64(h, i)  // Writes index of this output.
	h.Sum(id[:0])

	// Sanity check - verify that the optimized code is always returning the
	// same ids as the unoptimized code.
	if build.DEBUG {
		verificationID := SiaClassiccoinOutputID(crypto.HashAll(
			SpecifierSiaClassiccoinOutput,
			t.SiaClassiccoinInputs,
			t.SiaClassiccoinOutputs,
			t.FileContracts,
			t.FileContractRevisions,
			t.StorageProofs,
			t.SiaClassicfundInputs,
			t.SiaClassicfundOutputs,
			t.MinerFees,
			t.ArbitraryData,
			i,
		))
		if id != verificationID {
			panic("SiaClassiccoinOutputID is not marshalling correctly")
		}
	}

	return id
}

// FileContractID returns the ID of a file contract at the given index, which
// is calculated by hashing the concatenation of the FileContract Specifier,
// all of the fields in the transaction (except the signatures), and the
// contract index.
func (t Transaction) FileContractID(i uint64) FileContractID {
	var id FileContractID
	h := crypto.NewHash()
	h.Write(SpecifierFileContract[:])
	t.marshalSiaClassicNoSignatures(h) // Encode non-signature fields into hash.
	encoding.WriteUint64(h, i)  // Writes index of this output.
	h.Sum(id[:0])

	// Sanity check - verify that the optimized code is always returning the
	// same ids as the unoptimized code.
	if build.DEBUG {
		verificationID := FileContractID(crypto.HashAll(
			SpecifierFileContract,
			t.SiaClassiccoinInputs,
			t.SiaClassiccoinOutputs,
			t.FileContracts,
			t.FileContractRevisions,
			t.StorageProofs,
			t.SiaClassicfundInputs,
			t.SiaClassicfundOutputs,
			t.MinerFees,
			t.ArbitraryData,
			i,
		))
		if id != verificationID {
			panic("FileContractID is not marshalling correctly")
		}
	}

	return id
}

// SiaClassicfundOutputID returns the ID of a SiaClassicfundOutput at the given index, which
// is calculated by hashing the concatenation of the SiaClassicfundOutput Specifier,
// all of the fields in the transaction (except the signatures), and output
// index.
func (t Transaction) SiaClassicfundOutputID(i uint64) SiaClassicfundOutputID {
	var id SiaClassicfundOutputID
	h := crypto.NewHash()
	h.Write(SpecifierSiaClassicfundOutput[:])
	t.marshalSiaClassicNoSignatures(h) // Encode non-signature fields into hash.
	encoding.WriteUint64(h, i)  // Writes index of this output.
	h.Sum(id[:0])

	// Sanity check - verify that the optimized code is always returning the
	// same ids as the unoptimized code.
	if build.DEBUG {
		verificationID := SiaClassicfundOutputID(crypto.HashAll(
			SpecifierSiaClassicfundOutput,
			t.SiaClassiccoinInputs,
			t.SiaClassiccoinOutputs,
			t.FileContracts,
			t.FileContractRevisions,
			t.StorageProofs,
			t.SiaClassicfundInputs,
			t.SiaClassicfundOutputs,
			t.MinerFees,
			t.ArbitraryData,
			i,
		))
		if id != verificationID {
			panic("SiaClassicfundOutputID is not marshalling correctly")
		}
	}
	return id
}

// SiaClassiccoinOutputSum returns the sum of all the siaclassiccoin outputs in the
// transaction, which must match the sum of all the siaclassiccoin inputs. SiaClassiccoin
// outputs created by storage proofs and siaclassicfund outputs are not considered, as
// they were considered when the contract responsible for funding them was
// created.
func (t Transaction) SiaClassiccoinOutputSum() (sum Currency) {
	// Add the siaclassiccoin outputs.
	for _, sco := range t.SiaClassiccoinOutputs {
		sum = sum.Add(sco.Value)
	}

	// Add the file contract payouts.
	for _, fc := range t.FileContracts {
		sum = sum.Add(fc.Payout)
	}

	// Add the miner fees.
	for _, fee := range t.MinerFees {
		sum = sum.Add(fee)
	}

	return
}

// SiaClassicClaimOutputID returns the ID of the SiaClassiccoinOutput that is created when
// the siaclassicfund output is spent. The ID is the hash the SiaClassicfundOutputID.
func (id SiaClassicfundOutputID) SiaClassicClaimOutputID() SiaClassiccoinOutputID {
	return SiaClassiccoinOutputID(crypto.HashObject(id))
}

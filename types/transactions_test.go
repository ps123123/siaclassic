package types

import (
	"testing"

	"gitlab.com/moderndevgroup/SiaClassic/crypto"
)

// TestTransactionIDs probes all of the ID functions of the Transaction type.
func TestIDs(t *testing.T) {
	// Create every type of ID using empty fields.
	txn := Transaction{
		SiaClassiccoinOutputs: []SiaClassiccoinOutput{{}},
		FileContracts:  []FileContract{{}},
		SiaClassicfundOutputs: []SiaClassicfundOutput{{}},
	}
	tid := txn.ID()
	scoid := txn.SiaClassiccoinOutputID(0)
	fcid := txn.FileContractID(0)
	spidT := fcid.StorageProofOutputID(ProofValid, 0)
	spidF := fcid.StorageProofOutputID(ProofMissed, 0)
	sfoid := txn.SiaClassicfundOutputID(0)
	scloid := sfoid.SiaClassicClaimOutputID()

	// Put all of the ids into a slice.
	var ids []crypto.Hash
	ids = append(ids,
		crypto.Hash(tid),
		crypto.Hash(scoid),
		crypto.Hash(fcid),
		crypto.Hash(spidT),
		crypto.Hash(spidF),
		crypto.Hash(sfoid),
		crypto.Hash(scloid),
	)

	// Check that each id is unique.
	knownIDs := make(map[crypto.Hash]struct{})
	for i, id := range ids {
		_, exists := knownIDs[id]
		if exists {
			t.Error("id repeat for index", i)
		}
		knownIDs[id] = struct{}{}
	}
}

// TestTransactionSiaClassiccoinOutputSum probes the SiaClassiccoinOutputSum method of the
// Transaction type.
func TestTransactionSiaClassiccoinOutputSum(t *testing.T) {
	// Create a transaction with all types of siaclassiccoin outputs.
	txn := Transaction{
		SiaClassiccoinOutputs: []SiaClassiccoinOutput{
			{Value: NewCurrency64(1)},
			{Value: NewCurrency64(20)},
		},
		FileContracts: []FileContract{
			{Payout: NewCurrency64(300)},
			{Payout: NewCurrency64(4000)},
		},
		MinerFees: []Currency{
			NewCurrency64(50000),
			NewCurrency64(600000),
		},
	}
	if txn.SiaClassiccoinOutputSum().Cmp(NewCurrency64(654321)) != 0 {
		t.Error("wrong siaclassiccoin output sum was calculated, got:", txn.SiaClassiccoinOutputSum())
	}
}

package consensus

/*
import (
	"testing"

	"gitlab.com/moderndevgroup/SiaClassic/modules"
	"gitlab.com/moderndevgroup/SiaClassic/types"
)

// TestApplySiaClassiccoinInputs probes the applySiaClassiccoinInputs method of the consensus
// set.
func TestApplySiaClassiccoinInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	// Create a consensus set and get it to 3 siaclassiccoin outputs. The consensus
	// set starts with 2 siaclassiccoin outputs, mining a block will add another.
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()
	b, _ := cst.miner.FindBlock()
	err = cst.cs.AcceptBlock(b)
	if err != nil {
		t.Fatal(err)
	}

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Fetch the output id's of each siaclassiccoin output in the consensus set.
	var ids []types.SiaClassiccoinOutputID
	cst.cs.db.forEachSiaClassiccoinOutputs(func(id types.SiaClassiccoinOutputID, sco types.SiaClassiccoinOutput) {
		ids = append(ids, id)
	})

	// Apply a transaction with a single siaclassiccoin input.
	txn := types.Transaction{
		SiaClassiccoinInputs: []types.SiaClassiccoinInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applySiaClassiccoinInputs(pb, txn)
	exists := cst.cs.db.inSiaClassiccoinOutputs(ids[0])
	if exists {
		t.Error("Failed to conusme a siaclassiccoin output")
	}
	if cst.cs.db.lenSiaClassiccoinOutputs() != 2 {
		t.Error("siaclassiccoin outputs not correctly updated")
	}
	if len(pb.SiaClassiccoinOutputDiffs) != 1 {
		t.Error("block node was not updated for single transaction")
	}
	if pb.SiaClassiccoinOutputDiffs[0].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when consuming a siaclassiccoin output")
	}
	if pb.SiaClassiccoinOutputDiffs[0].ID != ids[0] {
		t.Error("wrong id used when consuming a siaclassiccoin output")
	}

	// Apply a transaction with two siaclassiccoin inputs.
	txn = types.Transaction{
		SiaClassiccoinInputs: []types.SiaClassiccoinInput{
			{ParentID: ids[1]},
			{ParentID: ids[2]},
		},
	}
	cst.cs.applySiaClassiccoinInputs(pb, txn)
	if cst.cs.db.lenSiaClassiccoinOutputs() != 0 {
		t.Error("failed to consume all siaclassiccoin outputs in the consensus set")
	}
	if len(pb.SiaClassiccoinOutputDiffs) != 3 {
		t.Error("processed block was not updated for single transaction")
	}
}

// TestMisuseApplySiaClassiccoinInputs misuses applySiaClassiccoinInput and checks that a
// panic was triggered.
func TestMisuseApplySiaClassiccoinInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Fetch the output id's of each siaclassiccoin output in the consensus set.
	var ids []types.SiaClassiccoinOutputID
	cst.cs.db.forEachSiaClassiccoinOutputs(func(id types.SiaClassiccoinOutputID, sco types.SiaClassiccoinOutput) {
		ids = append(ids, id)
	})

	// Apply a transaction with a single siaclassiccoin input.
	txn := types.Transaction{
		SiaClassiccoinInputs: []types.SiaClassiccoinInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applySiaClassiccoinInputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting error after corrupting database")
		}
	}()
	cst.cs.applySiaClassiccoinInputs(pb, txn)
}

// TestApplySiaClassiccoinOutputs probes the applySiaClassiccoinOutput method of the
// consensus set.
func TestApplySiaClassiccoinOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single siaclassiccoin output.
	txn := types.Transaction{
		SiaClassiccoinOutputs: []types.SiaClassiccoinOutput{{}},
	}
	cst.cs.applySiaClassiccoinOutputs(pb, txn)
	scoid := txn.SiaClassiccoinOutputID(0)
	exists := cst.cs.db.inSiaClassiccoinOutputs(scoid)
	if !exists {
		t.Error("Failed to create siaclassiccoin output")
	}
	if cst.cs.db.lenSiaClassiccoinOutputs() != 3 { // 3 because createConsensusSetTester has 2 initially.
		t.Error("siaclassiccoin outputs not correctly updated")
	}
	if len(pb.SiaClassiccoinOutputDiffs) != 1 {
		t.Error("block node was not updated for single element transaction")
	}
	if pb.SiaClassiccoinOutputDiffs[0].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when creating a siaclassiccoin output")
	}
	if pb.SiaClassiccoinOutputDiffs[0].ID != scoid {
		t.Error("wrong id used when creating a siaclassiccoin output")
	}

	// Apply a transaction with 2 siaclassiccoin outputs.
	txn = types.Transaction{
		SiaClassiccoinOutputs: []types.SiaClassiccoinOutput{
			{Value: types.NewCurrency64(1)},
			{Value: types.NewCurrency64(2)},
		},
	}
	cst.cs.applySiaClassiccoinOutputs(pb, txn)
	scoid0 := txn.SiaClassiccoinOutputID(0)
	scoid1 := txn.SiaClassiccoinOutputID(1)
	exists = cst.cs.db.inSiaClassiccoinOutputs(scoid0)
	if !exists {
		t.Error("Failed to create siaclassiccoin output")
	}
	exists = cst.cs.db.inSiaClassiccoinOutputs(scoid1)
	if !exists {
		t.Error("Failed to create siaclassiccoin output")
	}
	if cst.cs.db.lenSiaClassiccoinOutputs() != 5 { // 5 because createConsensusSetTester has 2 initially.
		t.Error("siaclassiccoin outputs not correctly updated")
	}
	if len(pb.SiaClassiccoinOutputDiffs) != 3 {
		t.Error("block node was not updated correctly")
	}
}

// TestMisuseApplySiaClassiccoinOutputs misuses applySiaClassiccoinOutputs and checks that a
// panic was triggered.
func TestMisuseApplySiaClassiccoinOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single siaclassiccoin output.
	txn := types.Transaction{
		SiaClassiccoinOutputs: []types.SiaClassiccoinOutput{{}},
	}
	cst.cs.applySiaClassiccoinOutputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("no panic occurred when misusing applySiaClassiccoinInput")
		}
	}()
	cst.cs.applySiaClassiccoinOutputs(pb, txn)
}

// TestApplyFileContracts probes the applyFileContracts method of the
// consensus set.
func TestApplyFileContracts(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single file contract.
	txn := types.Transaction{
		FileContracts: []types.FileContract{{}},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid := txn.FileContractID(0)
	exists := cst.cs.db.inFileContracts(fcid)
	if !exists {
		t.Error("Failed to create file contract")
	}
	if cst.cs.db.lenFileContracts() != 1 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 1 {
		t.Error("block node was not updated for single element transaction")
	}
	if pb.FileContractDiffs[0].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when creating a file contract")
	}
	if pb.FileContractDiffs[0].ID != fcid {
		t.Error("wrong id used when creating a file contract")
	}

	// Apply a transaction with 2 file contracts.
	txn = types.Transaction{
		FileContracts: []types.FileContract{
			{Payout: types.NewCurrency64(1)},
			{Payout: types.NewCurrency64(300e3)},
		},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid0 := txn.FileContractID(0)
	fcid1 := txn.FileContractID(1)
	exists = cst.cs.db.inFileContracts(fcid0)
	if !exists {
		t.Error("Failed to create file contract")
	}
	exists = cst.cs.db.inFileContracts(fcid1)
	if !exists {
		t.Error("Failed to create file contract")
	}
	if cst.cs.db.lenFileContracts() != 3 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 3 {
		t.Error("block node was not updated correctly")
	}
	if cst.cs.siaclassicfundPool.Cmp64(10e3) != 0 {
		t.Error("siaclassicfund pool did not update correctly upon creation of a file contract")
	}
}

// TestMisuseApplyFileContracts misuses applyFileContracts and checks that a
// panic was triggered.
func TestMisuseApplyFileContracts(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single file contract.
	txn := types.Transaction{
		FileContracts: []types.FileContract{{}},
	}
	cst.cs.applyFileContracts(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("no panic occurred when misusing applySiaClassiccoinInput")
		}
	}()
	cst.cs.applyFileContracts(pb, txn)
}

// TestApplyFileContractRevisions probes the applyFileContractRevisions method
// of the consensus set.
func TestApplyFileContractRevisions(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with two file contracts - that way there is
	// something to revise.
	txn := types.Transaction{
		FileContracts: []types.FileContract{
			{},
			{Payout: types.NewCurrency64(1)},
		},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid0 := txn.FileContractID(0)
	fcid1 := txn.FileContractID(1)

	// Apply a single file contract revision.
	txn = types.Transaction{
		FileContractRevisions: []types.FileContractRevision{
			{
				ParentID:    fcid0,
				NewFileSize: 1,
			},
		},
	}
	cst.cs.applyFileContractRevisions(pb, txn)
	exists := cst.cs.db.inFileContracts(fcid0)
	if !exists {
		t.Error("Revision killed a file contract")
	}
	fc := cst.cs.db.getFileContracts(fcid0)
	if fc.FileSize != 1 {
		t.Error("file contract filesize not properly updated")
	}
	if cst.cs.db.lenFileContracts() != 2 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 4 { // 2 creating the initial contracts, 1 to remove the old, 1 to add the revision.
		t.Error("block node was not updated for single element transaction")
	}
	if pb.FileContractDiffs[2].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when revising a file contract")
	}
	if pb.FileContractDiffs[3].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when revising a file contract")
	}
	if pb.FileContractDiffs[2].ID != fcid0 {
		t.Error("wrong id used when revising a file contract")
	}
	if pb.FileContractDiffs[3].ID != fcid0 {
		t.Error("wrong id used when revising a file contract")
	}

	// Apply a transaction with 2 file contract revisions.
	txn = types.Transaction{
		FileContractRevisions: []types.FileContractRevision{
			{
				ParentID:    fcid0,
				NewFileSize: 2,
			},
			{
				ParentID:    fcid1,
				NewFileSize: 3,
			},
		},
	}
	cst.cs.applyFileContractRevisions(pb, txn)
	exists = cst.cs.db.inFileContracts(fcid0)
	if !exists {
		t.Error("Revision ate file contract")
	}
	fc0 := cst.cs.db.getFileContracts(fcid0)
	exists = cst.cs.db.inFileContracts(fcid1)
	if !exists {
		t.Error("Revision ate file contract")
	}
	fc1 := cst.cs.db.getFileContracts(fcid1)
	if fc0.FileSize != 2 {
		t.Error("Revision not correctly applied")
	}
	if fc1.FileSize != 3 {
		t.Error("Revision not correctly applied")
	}
	if cst.cs.db.lenFileContracts() != 2 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 8 {
		t.Error("block node was not updated correctly")
	}
}

// TestMisuseApplyFileContractRevisions misuses applyFileContractRevisions and
// checks that a panic was triggered.
func TestMisuseApplyFileContractRevisions(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Trigger a panic from revising a nonexistent file contract.
	defer func() {
		r := recover()
		if r != errNilItem {
			t.Error("no panic occurred when misusing applySiaClassiccoinInput")
		}
	}()
	txn := types.Transaction{
		FileContractRevisions: []types.FileContractRevision{{}},
	}
	cst.cs.applyFileContractRevisions(pb, txn)
}

// TestApplyStorageProofs probes the applyStorageProofs method of the consensus
// set.
func TestApplyStorageProofs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Apply a transaction with two file contracts - there is a reason to
	// create a storage proof.
	txn := types.Transaction{
		FileContracts: []types.FileContract{
			{
				Payout: types.NewCurrency64(300e3),
				ValidProofOutputs: []types.SiaClassiccoinOutput{
					{Value: types.NewCurrency64(290e3)},
				},
			},
			{},
			{
				Payout: types.NewCurrency64(600e3),
				ValidProofOutputs: []types.SiaClassiccoinOutput{
					{Value: types.NewCurrency64(280e3)},
					{Value: types.NewCurrency64(300e3)},
				},
			},
		},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid0 := txn.FileContractID(0)
	fcid1 := txn.FileContractID(1)
	fcid2 := txn.FileContractID(2)

	// Apply a single storage proof.
	txn = types.Transaction{
		StorageProofs: []types.StorageProof{{ParentID: fcid0}},
	}
	cst.cs.applyStorageProofs(pb, txn)
	exists := cst.cs.db.inFileContracts(fcid0)
	if exists {
		t.Error("Storage proof did not disable a file contract.")
	}
	if cst.cs.db.lenFileContracts() != 2 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 4 { // 3 creating the initial contracts, 1 for the storage proof.
		t.Error("block node was not updated for single element transaction")
	}
	if pb.FileContractDiffs[3].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when revising a file contract")
	}
	if pb.FileContractDiffs[3].ID != fcid0 {
		t.Error("wrong id used when revising a file contract")
	}
	spoid0 := fcid0.StorageProofOutputID(types.ProofValid, 0)
	exists = cst.cs.db.inDelayedSiaClassiccoinOutputsHeight(pb.Height+types.MaturityDelay, spoid0)
	if !exists {
		t.Error("storage proof output not created after applying a storage proof")
	}
	sco := cst.cs.db.getDelayedSiaClassiccoinOutputs(pb.Height+types.MaturityDelay, spoid0)
	if sco.Value.Cmp64(290e3) != 0 {
		t.Error("storage proof output was created with the wrong value")
	}

	// Apply a transaction with 2 storage proofs.
	txn = types.Transaction{
		StorageProofs: []types.StorageProof{
			{ParentID: fcid1},
			{ParentID: fcid2},
		},
	}
	cst.cs.applyStorageProofs(pb, txn)
	exists = cst.cs.db.inFileContracts(fcid1)
	if exists {
		t.Error("Storage proof failed to consume file contract.")
	}
	exists = cst.cs.db.inFileContracts(fcid2)
	if exists {
		t.Error("storage proof did not consume file contract")
	}
	if cst.cs.db.lenFileContracts() != 0 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 6 {
		t.Error("block node was not updated correctly")
	}
	spoid1 := fcid1.StorageProofOutputID(types.ProofValid, 0)
	exists = cst.cs.db.inSiaClassiccoinOutputs(spoid1)
	if exists {
		t.Error("output created when file contract had no corresponding output")
	}
	spoid2 := fcid2.StorageProofOutputID(types.ProofValid, 0)
	exists = cst.cs.db.inDelayedSiaClassiccoinOutputsHeight(pb.Height+types.MaturityDelay, spoid2)
	if !exists {
		t.Error("no output created by first output of file contract")
	}
	sco = cst.cs.db.getDelayedSiaClassiccoinOutputs(pb.Height+types.MaturityDelay, spoid2)
	if sco.Value.Cmp64(280e3) != 0 {
		t.Error("first siaclassiccoin output created has wrong value")
	}
	spoid3 := fcid2.StorageProofOutputID(types.ProofValid, 1)
	exists = cst.cs.db.inDelayedSiaClassiccoinOutputsHeight(pb.Height+types.MaturityDelay, spoid3)
	if !exists {
		t.Error("second output not created for storage proof")
	}
	sco = cst.cs.db.getDelayedSiaClassiccoinOutputs(pb.Height+types.MaturityDelay, spoid3)
	if sco.Value.Cmp64(300e3) != 0 {
		t.Error("second siaclassiccoin output has wrong value")
	}
	if cst.cs.siaclassicfundPool.Cmp64(30e3) != 0 {
		t.Error("siaclassicfund pool not being added up correctly")
	}
}

// TestNonexistentStorageProof applies a storage proof which points to a
// nonextentent parent.
func TestNonexistentStorageProof(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Trigger a panic by applying a storage proof for a nonexistent file
	// contract.
	defer func() {
		r := recover()
		if r != errNilItem {
			t.Error("no panic occurred when misusing applySiaClassiccoinInput")
		}
	}()
	txn := types.Transaction{
		StorageProofs: []types.StorageProof{{}},
	}
	cst.cs.applyStorageProofs(pb, txn)
}

// TestDuplicateStorageProof applies a storage proof which has already been
// applied.
func TestDuplicateStorageProof(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Create a file contract for the storage proof to prove.
	txn0 := types.Transaction{
		FileContracts: []types.FileContract{
			{
				Payout: types.NewCurrency64(300e3),
				ValidProofOutputs: []types.SiaClassiccoinOutput{
					{Value: types.NewCurrency64(290e3)},
				},
			},
		},
	}
	cst.cs.applyFileContracts(pb, txn0)
	fcid := txn0.FileContractID(0)

	// Apply a single storage proof.
	txn1 := types.Transaction{
		StorageProofs: []types.StorageProof{{ParentID: fcid}},
	}
	cst.cs.applyStorageProofs(pb, txn1)

	// Trigger a panic by applying the storage proof again.
	defer func() {
		r := recover()
		if r != ErrDuplicateValidProofOutput {
			t.Error("failed to trigger ErrDuplicateValidProofOutput:", r)
		}
	}()
	cst.cs.applyFileContracts(pb, txn0) // File contract was consumed by the first proof.
	cst.cs.applyStorageProofs(pb, txn1)
}

// TestApplySiaClassicfundInputs probes the applySiaClassicfundInputs method of the consensus
// set.
func TestApplySiaClassicfundInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Fetch the output id's of each siaclassiccoin output in the consensus set.
	var ids []types.SiaClassicfundOutputID
	cst.cs.db.forEachSiaClassicfundOutputs(func(sfoid types.SiaClassicfundOutputID, sfo types.SiaClassicfundOutput) {
		ids = append(ids, sfoid)
	})

	// Apply a transaction with a single siaclassicfund input.
	txn := types.Transaction{
		SiaClassicfundInputs: []types.SiaClassicfundInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applySiaClassicfundInputs(pb, txn)
	exists := cst.cs.db.inSiaClassicfundOutputs(ids[0])
	if exists {
		t.Error("Failed to conusme a siaclassicfund output")
	}
	if cst.cs.db.lenSiaClassicfundOutputs() != 2 {
		t.Error("siaclassicfund outputs not correctly updated", cst.cs.db.lenSiaClassicfundOutputs())
	}
	if len(pb.SiaClassicfundOutputDiffs) != 1 {
		t.Error("block node was not updated for single transaction")
	}
	if pb.SiaClassicfundOutputDiffs[0].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when consuming a siaclassicfund output")
	}
	if pb.SiaClassicfundOutputDiffs[0].ID != ids[0] {
		t.Error("wrong id used when consuming a siaclassicfund output")
	}
	if cst.cs.db.lenDelayedSiaClassiccoinOutputsHeight(cst.cs.height()+types.MaturityDelay) != 2 { // 1 for a block subsidy, 1 for the siaclassicfund claim.
		t.Error("siaclassicfund claim was not created")
	}
}

// TestMisuseApplySiaClassicfundInputs misuses applySiaClassicfundInputs and checks that a
// panic was triggered.
func TestMisuseApplySiaClassicfundInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Fetch the output id's of each siaclassiccoin output in the consensus set.
	var ids []types.SiaClassicfundOutputID
	cst.cs.db.forEachSiaClassicfundOutputs(func(sfoid types.SiaClassicfundOutputID, sfo types.SiaClassicfundOutput) {
		ids = append(ids, sfoid)
	})

	// Apply a transaction with a single siaclassicfund input.
	txn := types.Transaction{
		SiaClassicfundInputs: []types.SiaClassicfundInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applySiaClassicfundInputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r != ErrMisuseApplySiaClassicfundInput {
			t.Error("no panic occurred when misusing applySiaClassiccoinInput")
			t.Error(r)
		}
	}()
	cst.cs.applySiaClassicfundInputs(pb, txn)
}

// TestApplySiaClassicfundOutputs probes the applySiaClassicfundOutputs method of the
// consensus set.
func TestApplySiaClassicfundOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()
	cst.cs.siaclassicfundPool = types.NewCurrency64(101)

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single siaclassicfund output.
	txn := types.Transaction{
		SiaClassicfundOutputs: []types.SiaClassicfundOutput{{}},
	}
	cst.cs.applySiaClassicfundOutputs(pb, txn)
	sfoid := txn.SiaClassicfundOutputID(0)
	exists := cst.cs.db.inSiaClassicfundOutputs(sfoid)
	if !exists {
		t.Error("Failed to create siaclassicfund output")
	}
	if cst.cs.db.lenSiaClassicfundOutputs() != 4 {
		t.Error("siaclassicfund outputs not correctly updated")
	}
	if len(pb.SiaClassicfundOutputDiffs) != 1 {
		t.Error("block node was not updated for single element transaction")
	}
	if pb.SiaClassicfundOutputDiffs[0].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when creating a siaclassicfund output")
	}
	if pb.SiaClassicfundOutputDiffs[0].ID != sfoid {
		t.Error("wrong id used when creating a siaclassicfund output")
	}
	if pb.SiaClassicfundOutputDiffs[0].SiaClassicfundOutput.ClaimStart.Cmp64(101) != 0 {
		t.Error("claim start set incorrectly when creating a siaclassicfund output")
	}

	// Apply a transaction with 2 siaclassiccoin outputs.
	txn = types.Transaction{
		SiaClassicfundOutputs: []types.SiaClassicfundOutput{
			{Value: types.NewCurrency64(1)},
			{Value: types.NewCurrency64(2)},
		},
	}
	cst.cs.applySiaClassicfundOutputs(pb, txn)
	sfoid0 := txn.SiaClassicfundOutputID(0)
	sfoid1 := txn.SiaClassicfundOutputID(1)
	exists = cst.cs.db.inSiaClassicfundOutputs(sfoid0)
	if !exists {
		t.Error("Failed to create siaclassicfund output")
	}
	exists = cst.cs.db.inSiaClassicfundOutputs(sfoid1)
	if !exists {
		t.Error("Failed to create siaclassicfund output")
	}
	if cst.cs.db.lenSiaClassicfundOutputs() != 6 {
		t.Error("siaclassicfund outputs not correctly updated")
	}
	if len(pb.SiaClassicfundOutputDiffs) != 3 {
		t.Error("block node was not updated for single element transaction")
	}
}

// TestMisuseApplySiaClassicfundOutputs misuses applySiaClassicfundOutputs and checks that a
// panic was triggered.
func TestMisuseApplySiaClassicfundOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single siaclassiccoin output.
	txn := types.Transaction{
		SiaClassicfundOutputs: []types.SiaClassicfundOutput{{}},
	}
	cst.cs.applySiaClassicfundOutputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r != ErrMisuseApplySiaClassicfundOutput {
			t.Error("no panic occurred when misusing applySiaClassicfundInput")
		}
	}()
	cst.cs.applySiaClassicfundOutputs(pb, txn)
}
*/
